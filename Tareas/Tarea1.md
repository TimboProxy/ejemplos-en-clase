# **Un cuento de pájaros sin voz**
_Primero voló el avaro pelícano,_
_ansioso de ser recompensado,_
_moviendo sus alas blancas._

_Luego partió una_ 
_silenciosa paloma,_
_volando detrás del pelícano,_ 
_aún más lejano._

## **Autor:** Desconocido